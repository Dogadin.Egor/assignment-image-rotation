#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "util.h"
#include "stream_bmp.h"
#include "rotate_bmp.h"
#include "stream_files.h"

// Догадин Егор R3235
int main( int argc, char** argv ) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );

    struct bmp_header h = { 0 };
    if (read_header_from_file( argv[1], &h )) {
        bmp_header_print( &h, stdout );
    }
    else {
        err( "Failed to open BMP file or reading header.\n" );
    }

    struct image image;

    FILE *in = NULL;
    if (open_file(&in, argv[1], "rb") != OPEN_OK)
        err("Errors occurred when opening the input file \n");

    switch (from_bmp(in, &image, &h)) {
	    case READ_OK:
		    break;
	    case READ_INVALID_SIGNATURE:
		    err("Invalid signature \n" );
	    case READ_INVALID_BITS:
		    err("Invalid count of bits \n" );
	    case READ_INVALID_HEADER:
		    err("Invalid header \n" );
	}

    if (close_file(&in) != CLOSE_OK)
        err("Errors occurred when closing the input file \n");

    struct image rotated_image = rotate(image);

    FILE *out = NULL;
    if (open_file(&out, "rotated_image.bmp", "wb") != OPEN_OK)
        err("Errors occurred when opening the output file \n");

    switch (to_bmp(out, &rotated_image)) {
	    case WRITE_OK:
		    break;
	    case WRITE_INVALID_PROPORTIONS:
		    err("Invalid proportions of output image \n" );
	    case WRITE_INVALID_DATA:
		    err("Invalid data of output image \n" );
	}

    if (close_file(&out) != CLOSE_OK)
        err("Errors occurred when closing the output file \n");

    free(image.data);
	free(rotated_image.data);
    return 0;
}

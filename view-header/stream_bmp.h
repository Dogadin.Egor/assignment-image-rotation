#ifndef STREAM_BMP_H
#define STREAM_BMP_H

#include <stdlib.h>

#include "bmp.h"

enum read_status  {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER
};

enum read_status from_bmp(FILE* in, struct image* const read, struct bmp_header const *header);

enum  write_status  {
	WRITE_OK = 0,
	WRITE_INVALID_PROPORTIONS,
    WRITE_INVALID_DATA
};

enum write_status to_bmp( FILE* out, struct image const* img);

#endif
#ifndef ROTATE_BMP_H
#define ROTATE_BMP_H

#include <stdlib.h>
#include "bmp.h"

struct image rotate(struct image const source);

#endif
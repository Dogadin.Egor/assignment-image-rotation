#include "stream_bmp.h"


enum read_status from_bmp(FILE* in, struct image* const read, struct bmp_header const *header) {
    if(header->bOffBits != sizeof(struct bmp_header))
		return READ_INVALID_HEADER;
	if(header->biBitCount != 24)
		return READ_INVALID_BITS;
    
    size_t padding = (4 - (header->biWidth * sizeof(struct pixel)) % 4) % 4;
    read->width =  header->biWidth;
    read->height = header->biHeight;
    read->data = malloc(sizeof(struct pixel) * read->width * read->height);


	fseek(in, header->bOffBits, SEEK_CUR);
	for (size_t height = 0; height < read->height; ++height) {
		for (size_t width = 0; width < read->width; ++width)
			fread(read->data + (read->width * height + width), sizeof(struct pixel), 1, in);
		fseek(in, padding, SEEK_CUR);
	}
	return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    if (img->height <= 0 || img->width <=0)
        return WRITE_INVALID_PROPORTIONS;
    if (!img->data)
        return WRITE_INVALID_DATA;

    size_t padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;
	struct bmp_header header = {
		.bfType = 0x4D42,
		.bfileSize =  img->width*img->height*sizeof(struct pixel) + padding*img->height,
		.bfReserved = 0,
		.bOffBits = sizeof(struct bmp_header),
		.biSize = 40,
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = 1,
		.biBitCount = 24,
		.biCompression = 0,
		.biSizeImage = 0,
		.biXPelsPerMeter = 3780,
		.biYPelsPerMeter = 3780,
		.biClrUsed = 0,
		.biClrImportant = 0
    };
    
    fwrite(&header, sizeof(struct bmp_header), 1, out);

	for (size_t height = 0; height < img->height; ++height) {
		for (size_t width = 0; width < img->width; ++width)
			fwrite(img->data + (img->width * height + width), sizeof(struct pixel), 1, out);
		fseek(out, padding, SEEK_CUR);
	}
    return WRITE_OK;
}
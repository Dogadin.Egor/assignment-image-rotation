#include "rotate_bmp.h"

struct image rotate(struct image const source) {
    struct image new_image = {.width = source.height, .height = source.width};
    new_image.data = malloc(sizeof(struct pixel) * new_image.width * new_image.height);

    for (size_t height = 0; height < new_image.height; ++height)
		for (size_t width = 0; width < new_image.width; ++width)
			new_image.data[new_image.width * height + width] = source.data[width*source.width + (new_image.height-1-height)];
    
    return new_image;
}
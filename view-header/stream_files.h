#ifndef STREAM_FILES_H
#define STREAM_FILES_H

#include <stdio.h>

enum open_status  {
	OPEN_OK = 0,
	OPEN_NULL_FILE
};

enum open_status open_file(FILE** in, const char *filename, const char *mode);

enum close_status  {
	CLOSE_OK = 0,
	CLOSE_ERROR
};

enum close_status close_file(FILE** out);

#endif